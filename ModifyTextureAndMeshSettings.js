var qualityLevel = 39;
var allTexture2Nodes = eon.FindByProgID("EONVisualNodes.Texture2.1");
var allMesh3PropertiesNodes = eon.FindByProgID("EONVisualNodes.Mesh3PropertiesNode.1");
for (var i=0; i<allTexture2Nodes.Count; i++)
{
	allTexture2Nodes.item(i).GetFieldByName("UseGroupSettings").value = false; //Disabled group settings
    allTexture2Nodes.item(i).GetFieldByName("MaxWidth").value = 2048; //Changed MaxWidth from 65536 to 2048
    allTexture2Nodes.item(i).GetFieldByName("MaxHeight").value = 2048; //Changed MaxHeight from 65536 to 2048
    allTexture2Nodes.item(i).GetFieldByName("Mipmap").value = true; //Changed Mipmap to true
    allTexture2Nodes.item(i).GetFieldByName("MinFilter").value = 5; //Changed MinFilter to Linear Mip Linear
    allTexture2Nodes.item(i).GetFieldByName("MagFilter").value = 1; //Changed MagFilter to Linear
    allTexture2Nodes.item(i).GetFieldByName("QualityLevel").value = qualityLevel; //Changed QualityLevel from 100 to ...
    allTexture2Nodes.item(i).GetFieldByName("DistributionFormat").value = 1; //Changed DistributionFormat from 0 to Auto
	allTexture2Nodes.item(i).GetFieldByName("AsynchronousLoad").value = true; //Enable AsynchronousLoad on all texture2 nodes
}
for (var i=0; i<allMesh3PropertiesNodes.Count; i++)
{
	allMesh3PropertiesNodes.item(i).GetFieldByName("AsynchronousLoad").value = true; //Enable AsynchronousLoad on all mesh3Properties nodes
}

/**
 * Finds recursively for nodes that have specified field ID and name.
 * @param {Number} fieldId ID of field to be compared.
 * @param {String} fieldName Name of field associated with field ID.
 * @param {SFNode} root The starting point of the search.
 * @param {Number} opt_maxDepth The maximum depth to search in sub-tree.
 * @return {Array<SFNode>} List of nodes that have specified field ID and name.
 */
function findNodesByFieldIdAndName(fieldId, fieldName, root, opt_maxDepth) {
	var result = []

	if (root == null) return result

	fieldId = parseInt(fieldId)
	if (isNaN(fieldId) || fieldId < 0) return result

	fieldName = (fieldName == null ? '' : String(fieldName))
	if (/^[\s\xa0]*$/.test(fieldName)) return result

	if (nodeHasFieldIdAndName(root, fieldId, fieldName)) result.push(root)

	if (!isNaN(opt_maxDepth) && opt_maxDepth <= 0) return result

	if (!isNaN(opt_maxDepth)) --opt_maxDepth

	var children = root.GetFieldByName('TreeChildren').value
	children.forEach(function(child) {
		Array.prototype.push.apply(result, findNodesByFieldIdAndName(fieldId, fieldName, child, opt_maxDepth))
	})

	return result
}

/**
 * Checks whether the passed node has specified field ID and name.
 * @param {SFNode} node Node to be checked against with.
 * @param {Number} fieldId ID of field to be compared.
 * @param {String} fieldName Name of field associated with field ID.
 */
function nodeHasFieldIdAndName(node, fieldId, fieldName) {
	if (node == null) return false

	fieldId = parseInt(fieldId)
	if (isNaN(fieldId) || fieldId < 0) return false

	fieldName = (fieldName == null ? '' : String(fieldName))
	if (/^[\s\xa0]*$/.test(fieldName)) return false

	if (fieldId >= node.GetFieldCount()) return false

	return node.GetField(fieldId).GetName() === fieldName
}
