var allLight2Nodes = eon.FindByProgID("EonD3D.Light2.1");
var allCameraNodes = eon.FindByProgID("EonD3D.CameraNode.1");
var allOrbitNavigationNodes = eon.FindByProgID("EonNodePack1.OrbitNavigation.1");
for (var i=0; i<allLight2Nodes.Count; i++)
{
	eon.DeleteNode(allLight2Nodes.item(i)); //delete all nodes with field id 13, and field name Attenuation
}
for (var i=0; i<allCameraNodes.Count; i++)
{
	eon.DeleteNode(allCameraNodes.item(i)); //delete all nodes with field id 12, and field name EyeSeparation
}
for (var i=0; i<allOrbitNavigationNodes.Count; i++)
{
	eon.DeleteNode(allOrbitNavigationNodes.item(i)); //delete all nodes with field id 12, and field name EyeSeparation
}

/**
 * Finds recursively for nodes that have specified field ID and name.
 * @param {Number} fieldId ID of field to be compared.
 * @param {String} fieldName Name of field associated with field ID.
 * @param {SFNode} root The starting point of the search.
 * @param {Number} opt_maxDepth The maximum depth to search in sub-tree.
 * @return {Array<SFNode>} List of nodes that have specified field ID and name.
 */
function findNodesByFieldIdAndName(fieldId, fieldName, root, opt_maxDepth) {
	var result = []

	if (root == null) return result

	fieldId = parseInt(fieldId)
	if (isNaN(fieldId) || fieldId < 0) return result

	fieldName = (fieldName == null ? '' : String(fieldName))
	if (/^[\s\xa0]*$/.test(fieldName)) return result

	if (nodeHasFieldIdAndName(root, fieldId, fieldName)) result.push(root)

	if (!isNaN(opt_maxDepth) && opt_maxDepth <= 0) return result

	if (!isNaN(opt_maxDepth)) --opt_maxDepth

	var children = root.GetFieldByName('TreeChildren').value
	children.forEach(function(child) {
		Array.prototype.push.apply(result, findNodesByFieldIdAndName(fieldId, fieldName, child, opt_maxDepth))
	})

	return result
}

/**
 * Checks whether the passed node has specified field ID and name.
 * @param {SFNode} node Node to be checked against with.
 * @param {Number} fieldId ID of field to be compared.
 * @param {String} fieldName Name of field associated with field ID.
 */
function nodeHasFieldIdAndName(node, fieldId, fieldName) {
	if (node == null) return false

	fieldId = parseInt(fieldId)
	if (isNaN(fieldId) || fieldId < 0) return false

	fieldName = (fieldName == null ? '' : String(fieldName))
	if (/^[\s\xa0]*$/.test(fieldName)) return false

	if (fieldId >= node.GetFieldCount()) return false

	return node.GetField(fieldId).GetName() === fieldName
}
