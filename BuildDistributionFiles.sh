#!/bin/bash

# make sure file extension is lowercase!!!

# Following prerequisites are expected:
#   Path to include where current EONContentTool.exe is located, normally something like
#      C:\Program Files\EON Reality\EON 9.14.5.15442\Bin
#   Path to include where this file is located, recommended is c:\Bin
#   Environment variable EONCONTENTTOOLUTILITIESDIR shall exist 
#      and point to folder where BuildDistributionFile.sh, ModifyTextureAndMeshSettings.js and RemoveUnwantedNodes.js are located


for dir in *; do
	if [[ -d $dir ]]; then

		echo "---------------------------------------------------->"
		echo "----> processing $dir"
		echo
 		pushd $dir >> /dev/null

		echo "----> Migrate"
		# This will 
		#	Tag file to current version
		#   Convert deprecated nodes
		#   Remove non-convertible nodes
		# Use the command
		# EONContentTool.exe help migrate
		# to see possible options
		EONContentTool.exe migrate --inplace *.eoz
		EONContentTool.exe migrate --inplace *.eop

		echo "----> Adapt to mobile"
		# This will
		#   Convert nodes to that have converters for the specified platforms.
		#   Remove unsupported nodes
		#   Run a script that will modify texture settings as a preparation for creation of distribution files
		EONContentTool.exe edit --inplace --adapt "android,ios" --script "$EONCONTENTTOOLUTILITIESDIR/ModifyTextureAndMeshSettings.js" *.eoz
		EONContentTool.exe edit --inplace --adapt "android,ios" --script "$EONCONTENTTOOLUTILITIESDIR/ModifyTextureAndMeshSettings.js" *.eop
	
		echo "----> Make Prototype"
		# This will create a prototype file with same name as the infile, but extension .eop
		EONContentTool.exe make-prototype --filename "%i" --inplace *.eoz
		
		echo "----> Remove specific unwanted nodes from prototype"
		# This will delete all the Camera, Light2, and Orbit nodes from .eop files 
		EONContentTool.exe edit --script "$EONCONTENTTOOLUTILITIESDIR/RemoveUnwantedNodes.js" --inplace *.eop	
		
		echo "----> Make Distribution"
 		EONContentTool.exe dist --inplace --platform "android" --compression-method "Store" *.eoz
 		EONContentTool.exe dist --inplace --platform "android" --compression-method "Store" *.eop
 		EONContentTool.exe dist --inplace --platform "ios" --filename "%i_iOS" --compression-method "Store" *.eoz
 		EONContentTool.exe dist --inplace --platform "ios" --filename "%i_iOS" --compression-method "Store" *.eop

 		popd >> /dev/null
	fi
done
